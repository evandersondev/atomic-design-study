import 'package:a_design/ui/atoms/button_custom.dart';
import 'package:a_design/ui/molecules/slide_image.dart';
import 'package:flutter/material.dart';

class CardCustom extends StatelessWidget {
  const CardCustom({
    required this.urls,
    required this.index,
    required this.prevImage,
    required this.nextImage,
  });

  final List<String> urls;
  final int index;
  final void Function() prevImage;
  final void Function() nextImage;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SlideImage(urls: urls, index: index),
          SizedBox(height: 12),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.46,
                child: ButtonCustom(
                  label: 'Prev',
                  onPress: prevImage,
                  color: Colors.red,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.46,
                child: ButtonCustom(
                  label: 'Next',
                  onPress: nextImage,
                  color: Colors.green,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
