import 'package:a_design/screens/home/home_screen.dart';
import 'package:a_design/screens/login/login_screen.dart';
import 'package:flutter/material.dart';

class ADesignApp extends StatelessWidget {
  const ADesignApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.amber[800],
          primaryColorLight: Colors.amber[800]),
      home: LoginScreen(),
    );
  }
}
