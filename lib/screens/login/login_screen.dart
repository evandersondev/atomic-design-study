import 'package:a_design/screens/home/home_screen.dart';
import 'package:a_design/ui/atoms/button_custom.dart';
import 'package:a_design/ui/atoms/title_custom.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: double.infinity,
          padding: EdgeInsets.all(16),
          color: Colors.grey[200],
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('EMAIL: admin'),
                  SizedBox(height: 4),
                  Text('Password: 123'),
                  SizedBox(height: 16),
                  TitleCustom(
                    text: 'LOGIN',
                    size: 64,
                  ),
                  SizedBox(height: 18),
                  TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                      hintText: 'Email',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 16),
                  TextField(
                    controller: passwordController,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 16),
                  ButtonCustom(
                      label: 'SUBMIT',
                      color: Colors.green[700],
                      onPress: () {
                        final email = emailController.text;
                        final password = passwordController.text;

                        if (email != '' &&
                            email == 'admin' &&
                            password != '' &&
                            password == '123') {
                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HomeScreen(),
                            ),
                          );
                        }
                      })
                ],
              ),
            ),
          )),
    );
  }
}
