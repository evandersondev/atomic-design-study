import 'package:a_design/screens/home/home_controller.dart';
import 'package:a_design/screens/login/login_screen.dart';
import 'package:a_design/ui/templates/list_custom.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  final homeController = HomeController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home Screen'),
        actions: [
          IconButton(
              onPressed: () => Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginScreen(),
                    ),
                  ),
              icon: Icon(Icons.logout))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: ValueListenableBuilder<int>(
          valueListenable: homeController.index,
          builder: (_, value, __) {
            return ListCustom(
              titleController: homeController.title,
              changeTitle: homeController.changeTitle,
              list: homeController.list,
              inputController: homeController.inputController,
              index: homeController.index.value,
              nextImage: homeController.nextImage,
              prevImage: homeController.prevImage,
            );
          },
        ),
      ),
    );
  }
}
